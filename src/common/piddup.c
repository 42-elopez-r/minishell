/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piddup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/09 16:59:45 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/09 17:14:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <common.h>

/*
** This function returns a heap allocated copy of the passed PID.
** Returns NULL in case of error.
*/

pid_t	*piddup(pid_t pid)
{
	pid_t	*pid_heap;

	if ((pid_heap = malloc(sizeof(pid_t))))
		*pid_heap = pid;
	else
		ft_perror("Can't allocate memory for PID");
	return (pid_heap);
}
