/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_stdin.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/08 19:32:06 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/09 16:58:14 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <parser.h>
#include <common.h>
#include <stdlib.h>

/*
** This function returns a heap allocated struct (allways the same one) with
** a duplication of the stdin file descriptor. If the file descriptor is closed,
** it will duplicate a new one.
*/

struct s_fd		*get_stdin(void)
{
	static struct s_fd *cur_stdin = NULL;

	if (!cur_stdin)
	{
		if (!(cur_stdin = malloc(sizeof(struct s_fd))))
		{
			ft_perror("Can't allocate file descriptior struct");
			return (NULL);
		}
		cur_stdin->is_open = FALSE;
	}
	if (!cur_stdin->is_open)
	{
		cur_stdin->fd = dup(0);
		cur_stdin->is_open = TRUE;
	}
	return (cur_stdin);
}
