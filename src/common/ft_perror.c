/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_perror.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <mzomeno-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/27 20:22:28 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/04 16:53:14 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <libft.h>
#include <errno.h>
#include <string.h>

/*
** perror(3) like function.
*/

void	ft_perror(char *str)
{
	int errno_copy;

	errno_copy = errno;
	if (*str)
	{
		ft_putstr_fd(str, 2);
		ft_putstr_fd(": ", 2);
	}
	ft_putendl_fd(strerror(errno_copy), 2);
}
