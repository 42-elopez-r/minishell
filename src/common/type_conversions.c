/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type_conversions.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/08 12:28:17 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/15 20:17:40 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <common.h>

/*
** This function takes a list of characters and converts it to a string
** allocated in the heap. In case of error returns NULL
*/

char	*list_to_string(t_list *list)
{
	char	*str;
	size_t	i;

	if (!(str = ft_calloc(sizeof(char), ft_lstsize(list) + 1)))
	{
		ft_perror("Can't convert list to string");
		return (NULL);
	}
	i = 0;
	while (list)
	{
		str[i++] = *((char*)list->content);
		list = list->next;
	}
	return (str);
}

/*
** This function takes a list of strings and converts it to a NULL terminated
** array allocated in the heap. In case of error returns NULL.
*/

char	**list_to_string_array(t_list *list)
{
	char	**array;
	size_t	i;

	if (!(array = ft_calloc(sizeof(char*), ft_lstsize(list) + 1)))
	{
		ft_perror("Can't convert list to array of strings");
		return (NULL);
	}
	i = 0;
	while (list)
	{
		array[i++] = list->content;
		list = list->next;
	}
	return (array);
}

/*
** This function takes a strings array and creates a list duplicating the
** strings on the heap.
*/

t_list	*string_array_to_list(char **array)
{
	t_list *list;

	list = NULL;
	while (array && *array)
	{
		ft_lstadd_back(&list, ft_lstnew(ft_strdup(*array)));
		array++;
	}
	return (list);
}
