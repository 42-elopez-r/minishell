/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_manipulation.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/08 12:34:36 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 10:53:58 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <common.h>
#include <stdlib.h>

/*
** This function accesses the node in the position index of the passed list
** and returns its content. If the index can't be accessed, it returns NULL.
*/

void			*lstindex(t_list *lst, size_t index)
{
	size_t	cur;

	cur = 0;
	while (lst)
	{
		if (cur == index)
			return (lst->content);
		cur++;
		lst = lst->next;
	}
	return (NULL);
}

/*
** This function deletes the last node of a list and returns the content
** the deleted node kept.
*/

void			*lstpop(t_list **lst)
{
	t_list	*cur;
	void	*content;

	content = NULL;
	if (!*lst)
		return (NULL);
	if (!(*lst)->next)
	{
		content = (*lst)->content;
		free(*lst);
		*lst = NULL;
	}
	cur = *lst;
	while (!content)
	{
		if (!cur->next->next)
		{
			content = cur->next->content;
			free(cur->next);
			cur->next = NULL;
		}
		cur = cur->next;
	}
	return (content);
}

/*
** This function stores a heap allocated copy of c into a new node at the back
** of the list.
*/

void			lstadd_back_char(t_list **list, char c)
{
	char *heap_c;

	if (!(heap_c = malloc(sizeof(char))))
	{
		ft_perror("Can't store character in list");
		return ;
	}
	*heap_c = c;
	ft_lstadd_back(list, ft_lstnew(heap_c));
}

/*
** This function orders a list alphabetically.
*/

void			lst_alpha_order(t_list **list)
{
	t_bool	yay_its_ordered;
	t_list	*sort;
	char	*tmp;

	yay_its_ordered = FALSE;
	while (yay_its_ordered == FALSE)
	{
		yay_its_ordered = TRUE;
		sort = *list;
		while (sort && sort->next)
		{
			if (ft_strcmp(sort->content, sort->next->content) > 0)
			{
				tmp = sort->content;
				sort->content = sort->next->content;
				sort->next->content = tmp;
				yay_its_ordered = FALSE;
			}
			sort = sort->next;
		}
	}
}

/*
** Copies a list... it is pretty self-explanatory
*/

t_list			*lst_cpy(t_list *list)
{
	t_list	*cpy;

	cpy = NULL;
	while (list)
	{
		ft_lstadd_back(&cpy, ft_lstnew(ft_strdup(list->content)));
		list = list->next;
	}
	return (cpy);
}
