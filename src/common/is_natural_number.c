/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_natural_number.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 19:04:31 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/16 20:28:32 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>

/*
** This function returns TRUE if the string is not empty and is only formed
** by digits.
*/

t_bool	is_natural_number(const char *str)
{
	t_bool	it_is_indeed;
	size_t	i;

	it_is_indeed = TRUE;
	i = 0;
	if (!str)
		return (FALSE);
	while (str[i] && it_is_indeed)
		if (!ft_isdigit(str[i++]))
			it_is_indeed = FALSE;
	return (it_is_indeed && i > 0);
}
