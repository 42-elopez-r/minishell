/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirections.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/03 11:59:43 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 11:11:18 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <common.h>
#include <executor.h>
#include <command_table.h>

static void	no_infile(struct s_loop_control *positions, int *infd,
						int last_pipe)
{
	if (!last_pipe && !positions->first)
		*infd = EMPTY_STDIN;
	else if (!last_pipe || positions->first)
		*infd = dup(0);
	else
		*infd = last_pipe;
}

/*
** Iterate the redir_list until an input file is found, open it and return its
** file descriptor.
** If open fails, return -1 and show an error.
** If no input file is found, takes input from stdin or from the last pipe
** else, closes the last pipe if it existed.
*/

int			get_infd(t_list *redir_list, struct s_loop_control *positions,
					int last_pipe)
{
	struct s_redirection	*redir;
	int						infd;

	infd = 0;
	while (redir_list && redir_list->content)
	{
		redir = redir_list->content;
		if (redir->is_input)
			infd = open(redir->filename, O_RDONLY, S_IRUSR);
		if (infd == -1)
		{
			ft_perror("Infile open failed");
			return (-1);
		}
		redir_list = redir_list->next;
	}
	if (infd == 0)
		no_infile(positions, &infd, last_pipe);
	else if (last_pipe)
		close(last_pipe);
	return (infd);
}

/*
** If it is not the last command, create a pipe to the next one
** else, redirect output to stdout
*/

static void	no_outfile(struct s_loop_control *positions, int *outfd,
						int pipe_out[2])
{
	if (!positions->last)
	{
		pipe(pipe_out);
		*outfd = pipe_out[1];
	}
	else
		*outfd = dup(1);
}

/*
** Iterate the redir_list, opens every file checking if the redirection appends
** (>>) or truncates (>) and return the file descriptor of the last one.
** If an open fails, the loop stops, returns -1 and shows an error.
** If no output file is found, return 0.
*/

int			get_outfd(t_list *redir_list, struct s_loop_control *positions,
						int pipe_out[2])
{
	struct s_redirection	*redir;
	int						outfd;

	outfd = 0;
	while (redir_list && redir_list->content)
	{
		redir = redir_list->content;
		if (!redir->is_input)
		{
			outfd = redir->overwrite ?
				open(redir->filename, O_CREAT | O_WRONLY | O_TRUNC,
				S_IRUSR | S_IWUSR) :
			open(redir->filename, O_CREAT | O_RDWR | O_APPEND,
				S_IRUSR | S_IWUSR);
			if (outfd == -1)
			{
				ft_perror("Outfile open failed");
				return (-1);
			}
		}
		redir_list = redir_list->next;
	}
	if (outfd == 0)
		no_outfile(positions, &outfd, pipe_out);
	return (outfd);
}
