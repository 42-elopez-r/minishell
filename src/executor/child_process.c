/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   child_process.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 18:53:09 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 00:55:16 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <common.h>
#include <executor.h>
#include <command_table.h>
#include <sys/stat.h>

int		run_child_process(int pipe_out[2], int fds_in_out[2],
							struct s_simple_command *simple_cmd,
							t_list **env_vars)
{
	int child_pid;

	child_pid = fork();
	if (child_pid == 0)
	{
		redirect_io(fds_in_out[1], fds_in_out[0], pipe_out[0]);
		execute_command(simple_cmd, env_vars);
	}
	else if (child_pid == -1)
		ft_perror("Can't spawn child process");
	return (child_pid);
}

/*
** Checks that the executable of the command exists and take its absolute path.
** Execute the command with execve, if it returns, something has gone wrong, so
** show an error and return.
*/

void	execute_command(struct s_simple_command *simple_cmd,
						t_list **env_vars)
{
	char		*cmd_path;
	char		**env_vars_array;
	t_builtin	builtin;

	if (!(env_vars_array = list_to_string_array(*env_vars)))
		exit(1);
	if (!(cmd_path = validate_command(simple_cmd, env_vars, &builtin)))
	{
		free(env_vars_array);
		exit(127);
	}
	else if (ft_strcmp(cmd_path, "builtin") == 0)
	{
		free(env_vars_array);
		exit(builtin(simple_cmd->arguments, env_vars));
	}
	execve(cmd_path, simple_cmd->arguments, env_vars_array);
	ft_putendl_fd("Something went wrong", 2);
}

/*
**	Dup fdin and close it.
**		In case it is a pipe, the write end was already closed in the previous
**		command by the parent process.
**	Dup fdout and close it.
**		If it's a pipe, close the read end.
*/

void	redirect_io(int fdin, int fdout, int read_end)
{
	int dev_null_fd;

	if (fdin != EMPTY_STDIN)
	{
		dup2(fdin, 0);
		close(fdin);
	}
	else
	{
		dev_null_fd = open("/dev/null", O_RDONLY);
		dup2(dev_null_fd, 0);
		close(dev_null_fd);
	}
	if (read_end)
		close(read_end);
	dup2(fdout, 1);
	close(fdout);
}
