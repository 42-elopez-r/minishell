/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_paths.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/18 19:33:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/19 02:50:00 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <command_table.h>
#include <dirent.h>
#include <errno.h>
#include <executor.h>
#include <stdio.h>
#include <sys/stat.h>

/*
** Given a command and a path, search the executable in the corresponding
** directory. Return with an error if the path cannot be accessed.
*/

t_bool			find_command(char *path, char *command)
{
	struct stat	buf;
	char		*full_path;
	t_bool		found;

	full_path = ft_strjoin(path, command);
	found = ((stat(full_path, &buf) == 0) && (buf.st_mode & S_IXUSR)) ?
		TRUE : FALSE;
	free(full_path);
	return (found);
}

static t_bool	check_path(char **cmd_path, char **cmd, char **paths, int it)
{
	*cmd_path = ft_strjoin(paths[it], "/");
	if (find_command(*cmd_path, *cmd) == TRUE)
	{
		delete_paths(paths);
		return (TRUE);
	}
	free(*cmd_path);
	*cmd_path = NULL;
	return (FALSE);
}

/*
** For every path in the PATH variable, check if the command in simple_cmd
** exists in that directory.
** Return with an error if the command isn't found in any path
*/

t_bool			search_paths(struct s_simple_command *simple_cmd,
							char **cmd_path, char **cmd, t_list *env_vars)
{
	char	**paths;
	size_t	it;

	*cmd = *(simple_cmd->arguments);
	paths = get_paths(env_vars);
	if (paths == NULL || *paths == NULL)
	{
		delete_paths(paths);
		return (FALSE);
	}
	it = 0;
	while (paths[it] != NULL)
	{
		if (check_path(cmd_path, cmd, paths, it) == TRUE)
			return (TRUE);
		it++;
	}
	ft_putstr_fd("minishell: ", 2);
	ft_putstr_fd(*cmd, 2);
	ft_putendl_fd(": command not found", 2);
	delete_paths(paths);
	return (FALSE);
}
