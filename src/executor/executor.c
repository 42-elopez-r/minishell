/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   executor.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <mzomeno-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/27 22:44:06 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 11:03:07 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <common.h>
#include <executor.h>
#include <command_table.h>
#include <environment_variables.h>

extern t_list	*g_running_pids;

/*
** This function waits for the exit of the process listed in g_running_pids
** and returns the exit status of the last one of them.
*/

static int		wait_for_pipeline(void)
{
	int		stat_loc;
	int		status;
	t_list	*rp_copy;
	int		wp;

	(void)wp;
	status = 0;
	rp_copy = g_running_pids;
	while (rp_copy)
	{
		wp = waitpid(*((pid_t*)rp_copy->content), &stat_loc, 0);
		if (WIFEXITED(stat_loc))
			status = WEXITSTATUS(stat_loc);
		else
			status = WIFSIGNALED(stat_loc) ? 128 + WTERMSIG(stat_loc) : 0;
		rp_copy = rp_copy->next;
	}
	return (status);
}

static void		initialize_pipeline(struct s_loop_control *positions,
									int in_out[2], t_list **pipeline,
									t_list *command_table)
{
	positions->first = TRUE;
	positions->last = FALSE;
	in_out[0] = 0;
	in_out[1] = dup(1);
	*pipeline = command_table->content;
	if (g_running_pids)
		ft_lstclear(&g_running_pids, free);
}

int				close_pipeline(pid_t pid, int std_out)
{
	int		last_exit_status;

	dup2(std_out, 1);
	close(std_out);
	if (pid <= -3)
		last_exit_status = -pid - 3;
	else
		last_exit_status = (pid == -2) ? 1 : wait_for_pipeline();
	return (last_exit_status);
}

/*
** This is the main executor's function.
** Create two pipes.
** Iterate the command table.
** 	Initialize the loop control flags.
**  Clear the list of pids from the last pipeline if it exists.
**  Iterate each pipeline and wait for its completion.
** 		Execute the command.
**		Toggle the even flag.
**		Permanently set the first flag as FALSE after the first execution.
** Returns the last exit status.
*/

int				executor(t_list *command_table, t_list **env_vars)
{
	t_list					*pipeline;
	struct s_loop_control	positions;
	pid_t					pid;
	int						io[2];
	int						last_exit_status;

	last_exit_status = 0;
	while (command_table)
	{
		initialize_pipeline(&positions, io, &pipeline, command_table);
		while (pipeline)
		{
			positions.last = (pipeline->next == NULL);
			pid = process_pipeline(pipeline, &positions, &(io[0]), env_vars);
			positions.first = FALSE;
			if (pid > 0)
				ft_lstadd_back(&g_running_pids, ft_lstnew(piddup(pid)));
			pipeline = pipeline->next;
		}
		last_exit_status = close_pipeline(pid, io[1]);
		command_table = command_table->next;
	}
	if (g_running_pids)
		ft_lstclear(&g_running_pids, free);
	return (last_exit_status);
}
