/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   paths.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/06 12:45:07 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/08 23:29:56 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <environment_variables.h>
#include <executor.h>

/*
** This function receives the list of environment variables and returns a
** NULL terminated array with the directories of the $PATH variable.
** Returns NULL on error.
*/

char	**get_paths(t_list *env_vars)
{
	char *path;
	char **paths;

	if (!(path = env_variable_get_value(env_vars, "PATH")))
		return (NULL);
	paths = ft_split(path, ':');
	free(path);
	return (paths);
}

/*
** This function frees the array (and its contents) returned by the above
** function.
*/

void	delete_paths(char **paths)
{
	size_t i;

	if (paths)
	{
		i = 0;
		while (paths[i])
			free(paths[i++]);
		free(paths);
	}
}
