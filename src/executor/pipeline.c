/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipeline.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/20 11:00:51 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 11:02:35 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <common.h>
#include <executor.h>
#include <command_table.h>
#include <environment_variables.h>

static int		prepare_pipes(int fds_in_out[2], int pipe_out[2], int *pipe_in,
								int pid)
{
	close(fds_in_out[1]);
	close(fds_in_out[0]);
	*pipe_in = pipe_out[0];
	return (pid);
}

/*
** Initialize the array for the pipe to 0.
** Sets the positions->last flag to TRUE if this is the last command of the
** pipeline (this info will be useful for the redirect_pipes function)
** Get the intput and output file descriptors.
** Create a subprocess and, inside it, perform the input/output redirections
** and execute the command.
** Show an error if the fork failed.
** Close the standard i/o file descriptors.
** If a pipe was created, set the next input to come from the current output.
** Returns the PID of the subprocess, which is -1 if the fork failed.
** It will return -2 if the redirections fails.
** If the return value is <= -3, then it executed a builtin with return code
** being [return value]*(-1)-3.
*/

pid_t			process_pipeline(t_list *pipeline,
									struct s_loop_control *positions,
									int *pipe_in, t_list **env_vars)
{
	pid_t					pid;
	struct s_simple_command	*simple_cmd;
	int						pipe_out[2];
	int						fds_in_out[2];
	t_builtin				builtin;

	pipe_out[0] = 0;
	pipe_out[1] = 0;
	simple_cmd = pipeline->content;
	fds_in_out[1] = get_infd(simple_cmd->redirections, positions, *pipe_in);
	fds_in_out[0] = get_outfd(simple_cmd->redirections, positions, pipe_out);
	if (fds_in_out[1] == -1 || fds_in_out[0] == -1)
		return (-2);
	if (positions->first && positions->last &&
			(builtin = search_builtin(simple_cmd->arguments[0])))
	{
		dup2(fds_in_out[1], 0);
		if (pipe_out[0])
			close(pipe_out[0]);
		dup2(fds_in_out[0], 1);
		pid = -3 - builtin(simple_cmd->arguments, env_vars);
	}
	else
		pid = run_child_process(pipe_out, fds_in_out, simple_cmd, env_vars);
	return (prepare_pipes(fds_in_out, pipe_out, pipe_in, pid));
}
