/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_commands.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <mzomeno-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 18:06:26 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/18 20:06:29 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <command_table.h>
#include <dirent.h>
#include <errno.h>
#include <executor.h>
#include <stdio.h>
#include <sys/stat.h>

/*
** Separate the command from its path and save each in its correspondent
** parameter (**cmd and **cmd_path).
** Check if the executable of the command exists in that path.
*/

static t_bool	valid_path(struct s_simple_command *simple_cmd, char **cmd_path,
							char **cmd)
{
	char *tmp;

	tmp = ft_strrchr(*(simple_cmd->arguments), '/');
	*cmd_path = ft_substr(*(simple_cmd->arguments), 0,
			tmp - *(simple_cmd->arguments) + 1);
	*cmd = ft_substr(*(simple_cmd->arguments),
			tmp - *(simple_cmd->arguments) + 1,
			ft_strlen(simple_cmd->arguments[0]) - ft_strlen(*cmd_path));
	return (find_command(*cmd_path, *cmd));
}

/*
** If the command contains slashes, look for the command in the specified
** path, else, check if its a builtin, else, look for the command in the paths
** of the PATH variable.
** In case the program was a builtin, it returns the string "builtin" and
** sets its exec_function in the builtin parameter (that otherwise is NULL).
*/

char			*validate_command(struct s_simple_command *simple_cmd,
									t_list **env_vars, t_builtin *builtin)
{
	char *cmd_path;
	char *cmd;

	if ((ft_strchr(*(simple_cmd->arguments), '/')))
	{
		if (valid_path(simple_cmd, &cmd_path, &cmd) == FALSE)
		{
			ft_putstr_fd("minishell: ", 2);
			ft_putstr_fd(*(simple_cmd->arguments), 2);
			ft_putendl_fd(": command not found", 2);
			return (NULL);
		}
	}
	else
	{
		if ((*builtin = search_builtin(*(simple_cmd->arguments))))
			return ("builtin");
		else if (search_paths(simple_cmd, &cmd_path, &cmd, *env_vars) == FALSE)
			return (NULL);
	}
	return (ft_strjoin(cmd_path, cmd));
}
