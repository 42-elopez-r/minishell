/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environment_builtins.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/13 18:58:38 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/20 11:03:47 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <builtins.h>
#include <environment_variables.h>
#include <stdlib.h>

/*
** The export builtin exec_function.
*/

int			exec_export(char **args, t_list **env_vars)
{
	t_list	*env_cpy;
	int		return_code;

	if (*(args + 1))
		return (export_something(args, env_vars));
	else
	{
		env_cpy = lst_cpy(*env_vars);
		return_code = export_nothing(&env_cpy);
		ft_lstclear(&env_cpy, free);
		return (return_code);
	}
}

/*
** The unset builtin exec_function.
*/

int			exec_unset(char **args, t_list **env_vars)
{
	while (*(++args))
		delete_variable(*args, env_vars);
	return (0);
}

/*
** The env builtin exec_function.
*/

int			exec_env(char **args, t_list **env_vars)
{
	t_list *iter;

	(void)args;
	iter = *env_vars;
	while (iter)
	{
		if (ft_strchr(iter->content, '='))
			ft_putendl_fd(iter->content, 1);
		iter = iter->next;
	}
	return (0);
}
