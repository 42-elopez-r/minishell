/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generic_builtins.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 15:30:10 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/18 17:33:10 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <builtins.h>
#include <stdio.h>

extern int	g_shell_return_code;

int		exec_echo(char **args, t_list **env_vars)
{
	t_bool	newline;

	(void)env_vars;
	newline = TRUE;
	if (*(++args) && ft_strcmp(*args, "-n") == 0)
	{
		newline = FALSE;
		args++;
	}
	while (*args)
	{
		ft_putstr_fd(*args, 1);
		if (*(++args))
			ft_putchar_fd(' ', 1);
	}
	if (newline == TRUE)
		ft_putchar_fd('\n', 1);
	return (0);
}

int		exec_exit(char **args, t_list **env_vars)
{
	(void)env_vars;
	if (!args[1])
	{
		g_shell_return_code = 0;
		return (0);
	}
	if (!is_natural_number(args[1]))
	{
		ft_putstr_fd("minishell: exit: ", 2);
		ft_putstr_fd(args[1], 2);
		ft_putstr_fd(": numeric value required", 2);
		g_shell_return_code = 2;
		return (0);
	}
	g_shell_return_code = ft_atoi(args[1]);
	return (0);
}
