/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export_things.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/20 10:56:54 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/20 11:05:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <builtins.h>
#include <environment_variables.h>
#include <stdlib.h>

/*
** Displays an error for a given export argument.
*/

static void	display_export_error(char *arg)
{
	ft_putstr_fd("minishell: export: `", 2);
	ft_putstr_fd(arg, 2);
	ft_putendl_fd("': not a valid identifier", 2);
}

/*
** This function performs the task of the export builtin when called with no
** arguments. Returns exit code
*/

int			export_nothing(t_list **env_vars)
{
	t_list	*env;
	char	*surrounded;
	char	*joined;

	env = *env_vars;
	lst_alpha_order(&env);
	while (env)
	{
		if ((surrounded = surround_var_value_quotes(env->content)))
		{
			if (!(joined = ft_strjoin("declare -x ", surrounded)))
			{
				ft_perror("Can't generate entry for export");
				free(surrounded);
				return (1);
			}
			free(surrounded);
			ft_putendl_fd(joined, 1);
			free(joined);
		}
		else
			return (1);
		env = env->next;
	}
	return (0);
}

/*
** This function performs the task of the export builtin when called WITH
** arguments. Returns exit code.
*/

int			export_something(char **args, t_list **env_vars)
{
	int		exit_code;
	char	*arg;
	t_bool	wrong_name;

	exit_code = 0;
	while (*(++args))
	{
		arg = *args;
		wrong_name = (ft_isalpha(*arg) || *arg == '_') ? FALSE : TRUE;
		while (!wrong_name && *arg && *arg != '=')
		{
			if (!ft_isalnum(*arg) && *arg != '_')
				wrong_name = TRUE;
			arg++;
		}
		if (wrong_name)
		{
			display_export_error(*args);
			exit_code = 1;
			continue;
		}
		(*arg == '=') ? update_var(env_vars, *args) : \
			reset_empty_var(*args, env_vars);
	}
	return (exit_code);
}
