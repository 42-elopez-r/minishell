/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_builtin.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 11:58:26 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/13 18:39:01 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <builtins.h>

/*
** This function looks for the right builtin exec_function given the name
** of the builtin. If it can't be found, it returns NULL.
*/

t_builtin		search_builtin(char *cmd)
{
	if (ft_strcmp(cmd, "echo") == 0)
		return (exec_echo);
	else if (ft_strcmp(cmd, "cd") == 0)
		return (exec_cd);
	else if (ft_strcmp(cmd, "pwd") == 0)
		return (exec_pwd);
	else if (ft_strcmp(cmd, "export") == 0)
		return (exec_export);
	else if (ft_strcmp(cmd, "unset") == 0)
		return (exec_unset);
	else if (ft_strcmp(cmd, "env") == 0)
		return (exec_env);
	else if (ft_strcmp(cmd, "exit") == 0)
		return (exec_exit);
	else
		return (NULL);
}
