/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dir_builtins.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 13:10:32 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 11:39:31 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <builtins.h>
#include <environment_variables.h>
#include <stdio.h>
#include <unistd.h>

#ifdef Darwin
# include <sys/syslimits.h>
#else
# include <linux/limits.h>
#endif

static int	update_pwds(t_list **env_vars, const char *var_prefix)
{
	char buffer[PATH_MAX];
	char *pwd_var;

	if (getcwd(buffer, PATH_MAX))
	{
		pwd_var = ft_strjoin(var_prefix, buffer);
		update_var(env_vars, pwd_var);
		free(pwd_var);
		return (0);
	}
	else
		return (1);
}

int			exec_cd(char **args, t_list **env_vars)
{
	char *dir;
	char *home;
	char buffer[PATH_MAX + 15];

	if (update_pwds(env_vars, "OLDPWD=") == 1)
		return (1);
	home = env_variable_get_value(*env_vars, "HOME");
	dir = args[1] ? args[1] : home;
	if ((chdir(dir) == -1))
	{
		ft_memcpy(buffer, "minishell: cd: ", 15);
		ft_memcpy(buffer + 15, dir, ft_strlen(dir) + 1);
		ft_perror(buffer);
		free(home);
		return (1);
	}
	free(home);
	if (update_pwds(env_vars, "PWD=") == 1)
		return (1);
	return (0);
}

int			exec_pwd(char **args, t_list **env_vars)
{
	char buffer[PATH_MAX];
	char *current_path;

	(void)args;
	(void)env_vars;
	if ((current_path = getcwd(buffer, PATH_MAX)))
	{
		ft_putendl_fd(current_path, 1);
		return (0);
	}
	return (1);
}
