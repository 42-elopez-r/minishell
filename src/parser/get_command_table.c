/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_command_table.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 19:19:31 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/16 19:04:31 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <environment_variables.h>
#include <parser.h>
#include <get_next_line.h>
#include <stdlib.h>

/*
** This function generates and returns a command table for the command "exit".
*/

static t_list	*exit_command_table(void)
{
	struct s_tokens	tokens;
	t_list			*command_table;

	ft_putstr_fd("\n", 1);
	tokens.toks = ft_lstnew(ft_strdup("exit"));
	tokens.i = 0;
	command_table = tokens_to_command_table(&tokens);
	ft_lstclear(&tokens.toks, free);
	return (command_table);
}

/*
** This is the principal parser's function. It will prompt the user for a
** command and return the command table. Requires the list of environment
** variables to perform variable expansion. It returns NULL in case of error.
*/

t_list			*get_command_table(t_list *env_vars, int last_exit_status)
{
	struct s_raw_command	rc;
	struct s_tokens			tokens;
	t_list					*command_table;
	char					*ps1;

	if ((ps1 = env_variable_get_value(env_vars, "PS1")))
	{
		ft_putstr_fd(ps1, 1);
		free(ps1);
	}
	if (get_next_line_ctrl_d(get_stdin()->fd, &rc.cmd))
		return (exit_command_table());
	rc.i = 0;
	if (!rc.cmd)
		return (NULL);
	tokens.toks = parse_tokens(&rc, env_vars, last_exit_status);
	tokens.i = 0;
	free(rc.cmd);
	if (!tokens.toks)
		return (NULL);
	command_table = tokens_to_command_table(&tokens);
	ft_lstclear(&tokens.toks, free);
	return (command_table);
}
