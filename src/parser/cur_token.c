/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cur_token.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/02 19:24:46 by elopez-r          #+#    #+#             */
/*   Updated: 2021/02/03 20:12:56 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <parser.h>

/*
** Returns the token in the current index plus the offset.
*/

char				*cur_token_off(struct s_tokens *tokens, size_t offset)
{
	return ((char*)lstindex(tokens->toks, tokens->i + offset));
}

/*
** Returns the current token (as set in tokens->i).
*/

char				*cur_token(struct s_tokens *tokens)
{
	return (cur_token_off(tokens, 0));
}
