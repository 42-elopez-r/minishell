/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokens_to_command_table.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/02 17:43:51 by elopez-r          #+#    #+#             */
/*   Updated: 2021/02/04 22:57:02 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <parser.h>

/*
** This function process the simple commands in a pipeline and returns them
** in a list.
*/

static t_list	*process_pipeline(struct s_tokens *tokens)
{
	t_list					*pipeline;
	struct s_simple_command	*simple_cmd;

	pipeline = NULL;
	while (TRUE)
	{
		if ((simple_cmd = process_simple_command(tokens)))
			ft_lstadd_back(&pipeline, ft_lstnew(simple_cmd));
		else
		{
			delete_pipeline(pipeline);
			return (NULL);
		}
		if (cur_token(tokens) &&
				cur_token(tokens)[0] == '\0' && cur_token(tokens)[1] == '|')
			tokens->i++;
		if (!cur_token(tokens) || (cur_token(tokens)[0] == '\0' &&
			(cur_token(tokens)[1] == ';' || cur_token(tokens)[1] == '|')))
			break ;
	}
	return (pipeline);
}

/*
** This function takes a list of tokens and creates from them a command table.
** Returns NULL in case of error.
*/

t_list			*tokens_to_command_table(struct s_tokens *tokens)
{
	t_list *cmd_table;
	t_list *pipeline;

	cmd_table = NULL;
	while (cur_token(tokens))
	{
		while (TRUE)
		{
			if ((pipeline = process_pipeline(tokens)))
				ft_lstadd_back(&cmd_table, ft_lstnew(pipeline));
			else
			{
				delete_command_table(cmd_table);
				return (NULL);
			}
			if (cur_token(tokens) &&
					cur_token(tokens)[0] == '\0' && cur_token(tokens)[1] == ';')
				tokens->i++;
			if (!cur_token(tokens) || (cur_token(tokens)[0] == '\0' &&
						cur_token(tokens)[1] == ';'))
				break ;
		}
	}
	return (cmd_table);
}
