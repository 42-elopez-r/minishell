/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_env_var_last_exit_status.c                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/01 19:27:26 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/16 20:25:24 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <common.h>
#include <environment_variables.h>
#include <parser.h>
#include <stdlib.h>

/*
** As the function's name suggest, it stores two zeroes in the characters list
** if it's empty and the value of the env variable is too. Yes, the norm.
*/

static void	store_double_zero_if_empty(t_list **characters, const char *value)
{
	if (ft_lstsize(*characters) == 0 && (!value || *value == '\0'))
	{
		lstadd_back_char(characters, '\0');
		lstadd_back_char(characters, '\0');
	}
}

/*
** This function receives the raw command string starting at the $ of an
** environment variable and it will add to the characters list the value of
** that variable expansion, advancing and skipping the variable name on the
** raw command string too.
*/

void		process_env_var(struct s_raw_command *rc, t_list **characters,
		t_list *env_vars)
{
	size_t	name_length;
	char	*name;
	char	*value;
	size_t	i;

	rc->i++;
	name_length = 0;
	while (ft_isalnum(rc->cmd[rc->i + name_length])
			|| rc->cmd[rc->i + name_length] == '_')
		name_length++;
	if (!(name = ft_calloc(sizeof(char), name_length + 1)))
	{
		ft_perror("Can't store environment variable name");
		rc->i += name_length;
		return ;
	}
	ft_memcpy(name, rc->cmd + rc->i, name_length);
	value = env_variable_get_value(env_vars, name);
	free(name);
	store_double_zero_if_empty(characters, value);
	i = 0;
	while (value && value[i])
		lstadd_back_char(characters, value[i++]);
	free(value);
	rc->i += name_length;
}

/*
** This function appends the exit status to the characters list. It also
** advances the raw command index to skip the "$?"
*/

void		process_last_exit_status(struct s_raw_command *rc,
		t_list **characters, int last_exit_status)
{
	char	*str_status;
	size_t	i;

	rc->i += 2;
	if (!(str_status = ft_itoa(last_exit_status)))
	{
		ft_perror("Can't store last exit status");
		return ;
	}
	i = 0;
	while (str_status[i])
		lstadd_back_char(characters, str_status[i++]);
	free(str_status);
}
