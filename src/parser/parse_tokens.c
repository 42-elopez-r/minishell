/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tokens.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/27 16:10:05 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/10 18:11:09 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <common.h>
#include <parser.h>
#include <get_next_line.h>
#include <stdlib.h>

/*
** This function increments the index of a s_raw_command skipping the
** spaces it finds.
*/

static void		skip_spaces(struct s_raw_command *rc)
{
	while (rc->cmd[rc->i] == ' ')
		rc->i++;
}

/*
** This function overwrites the tokens list with "exit", "2".
*/

static void		set_exit_2_tokens(t_list **tokens)
{
	if (tokens && *tokens)
		ft_lstclear(tokens, free);
	*tokens = ft_lstnew(ft_strdup("exit"));
	ft_lstadd_back(tokens, ft_lstnew(ft_strdup("2")));
}

/*
** This function prompts the user for another line of commands and parses
** the tokens on it, adding them to the already existing tokens list.
** If the get_next_line gets an EOF, it overwrites the tokens list with
** "exit", "2" and displays an error.
*/

static void		get_extra_tokens(t_list **tokens, struct s_raw_command *rc,
		t_list *env_vars, int last_exit_status)
{
	t_list	*extra_tokens;

	free(rc->cmd);
	rc->i = 0;
	ft_putstr_fd("> ", 1);
	if (get_next_line_ctrl_d(get_stdin()->fd, &rc->cmd))
	{
		set_exit_2_tokens(tokens);
		ft_putendl_fd("\nminishell: syntax error: unexpected end of file", 1);
	}
	else if (rc->cmd)
	{
		extra_tokens = parse_tokens(rc, env_vars, last_exit_status);
		ft_lstadd_back(tokens, extra_tokens);
	}
}

/*
** Given the raw string command, the list of environment variables and the exit
** status of the last pipeline, it returns a list with the tokens extracted
** after performing quoting and environment variable expansion. It will also
** prompt the user if the command is incomplete (if it reads then an EOF,
** it will return the tokens "exit", "2").
*/

t_list			*parse_tokens(struct s_raw_command *rc, t_list *env_vars,
		int last_exit_status)
{
	t_list	*tokens;
	char	*token;
	char	*last_token;
	t_bool	eof;

	tokens = NULL;
	eof = FALSE;
	while (!eof && rc->cmd && rc->cmd[rc->i])
	{
		skip_spaces(rc);
		if (rc->cmd[rc->i] && (token = process_token(rc, env_vars,
						last_exit_status, &eof)))
			ft_lstadd_back(&tokens, ft_lstnew(token));
		else if (eof)
			set_exit_2_tokens(&tokens);
	}
	if (tokens)
	{
		last_token = ft_lstlast(tokens)->content;
		if (!*last_token && ft_strcmp(last_token + 1, "|") == 0)
			get_extra_tokens(&tokens, rc, env_vars, last_exit_status);
	}
	return (tokens);
}
