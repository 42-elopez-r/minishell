/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_simple_command.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/02 19:03:37 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/17 20:23:00 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <command_table.h>
#include <libft.h>
#include <common.h>
#include <parser.h>
#include <stdlib.h>

/*
** This function does nothing. Is intended for use in ft_lstclear() when
** the list itself must be deleted but not its contents.
*/

static void					do_nothing(void *p)
{
	(void)p;
}

/*
** This function checks if the current token is a command delimiter ("|", ";").
** If it is, it issues an error and returns TRUE.
*/

static t_bool				check_starting_delimiter(struct s_tokens *tokens)
{
	if (cur_token(tokens)[0] == '\0' && (cur_token(tokens)[1] == ';' ||
				cur_token(tokens)[1] == '|'))
	{
		ft_putstr_fd("minishell: syntax error near unexpected token `", 2);
		ft_putstr_fd(cur_token(tokens) + 1, 2);
		ft_putendl_fd("'", 2);
		return (TRUE);
	}
	return (FALSE);
}

/*
** Assuming that the current token is a redirection, this function checks
** the presence of the associated filename and creates a s_redirection
** with the found data.
*/

static struct s_redirection	*process_redirection(struct s_tokens *tokens)
{
	struct s_redirection	*redir;
	char					*filename;

	filename = cur_token_off(tokens, 1);
	if (!filename || filename[0] == '\0')
	{
		ft_putstr_fd("minishell: syntax error near unexpected token `", 2);
		ft_putstr_fd(!filename ? "newline" : filename + 1, 2);
		ft_putendl_fd("'", 2);
		return (NULL);
	}
	if (!(redir = malloc(sizeof(struct s_redirection))))
	{
		ft_perror("Can't allocate s_redirection");
		return (NULL);
	}
	redir->filename = ft_strdup(filename);
	redir->is_input = FALSE;
	redir->overwrite = FALSE;
	if (ft_strcmp(cur_token(tokens) + 1, ">") == 0)
		redir->overwrite = TRUE;
	else if (ft_strcmp(cur_token(tokens) + 1, "<") == 0)
		redir->is_input = TRUE;
	tokens->i += 2;
	return (redir);
}

/*
** This function goes through the list of tokens adding the redirections to
** simple_cmd->redirections and the arguments to args. Returns TRUE upon
** success.
*/

static t_bool				actually_process_simple_command(t_list **args,
		struct s_simple_command *simple_cmd, struct s_tokens *tokens)
{
	struct s_redirection *redir;

	while (cur_token(tokens) && !(cur_token(tokens)[0] == '\0' &&
				(cur_token(tokens)[1] == ';' || cur_token(tokens)[1] == '|')))
	{
		if (cur_token(tokens)[0] == '\0' && cur_token(tokens)[1] == '\0')
			tokens->i++;
		else if (cur_token(tokens)[0] == '\0')
		{
			if ((redir = process_redirection(tokens)))
				ft_lstadd_back(&simple_cmd->redirections, ft_lstnew(redir));
			else
				return (FALSE);
		}
		else
		{
			ft_lstadd_back(args, ft_lstnew(ft_strdup(cur_token(tokens))));
			tokens->i++;
		}
	}
	return (TRUE);
}

/*
** This functions generates a s_simple_command from the tokens list advancing
** the tokens->i index. It returns NULL in case of error.
*/

struct s_simple_command		*process_simple_command(struct s_tokens *tokens)
{
	struct s_simple_command	*simple_cmd;
	t_list					*args;

	if (check_starting_delimiter(tokens))
		return (NULL);
	if (!(simple_cmd = malloc(sizeof(struct s_simple_command))))
	{
		ft_perror("Can't allocate s_simple_command");
		return (NULL);
	}
	simple_cmd->arguments = NULL;
	simple_cmd->redirections = NULL;
	args = NULL;
	if (!actually_process_simple_command(&args, simple_cmd, tokens) ||
			!(simple_cmd->arguments = list_to_string_array(args)))
	{
		if (args)
			ft_lstclear(&args, free);
		free(simple_cmd);
		return (NULL);
	}
	if (args)
		ft_lstclear(&args, do_nothing);
	return (simple_cmd);
}
