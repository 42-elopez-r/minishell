/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_list_to_string.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 18:08:33 by elopez-r          #+#    #+#             */
/*   Updated: 2021/02/03 18:22:50 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <common.h>
#include <parser.h>

/*
** This function wraps over list_to_string() to mind the case of shell
** operators hidden after a starting null byte.
*/

char	*token_list_to_string(t_list *characters)
{
	char *str_not_null;
	char *str_def;

	if (characters && *((char*)characters->content) == '\0')
	{
		str_not_null = list_to_string(characters->next);
		if (!(str_def = ft_calloc(sizeof(char), ft_strlen(str_not_null) + 2)))
		{
			ft_perror("Can't allocate shell operator token string");
			free(str_not_null);
			return (NULL);
		}
		ft_memcpy(str_def + 1, str_not_null, ft_strlen(str_not_null));
		free(str_not_null);
	}
	else
		str_def = list_to_string(characters);
	return (str_def);
}
