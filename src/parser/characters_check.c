/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   characters_check.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 17:39:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/02/08 18:27:07 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <common.h>
#include <parser.h>

/*
** Block of the backslash ending check inside characters_check_2.
** It didn't fit inside the function.
*/

static void	set_backslash_ending(struct s_raw_command *rc,
		struct s_process_token_data *td)
{
	td->backslash_ending = TRUE;
	rc->i++;
}

/*
** This function reads the firsts characters of the raw command and if it
** detects a quoting it enables the flag or processes the character and
** increments the index of the raw command string.
** Returns TRUE if a case was detected and action was performed.
** And yes, this function exists because this code doesn't fit into
** process_token() below.
*/

t_bool		characters_check_1(struct s_raw_command *rc,
		struct s_process_token_data *td)
{
	t_bool case_detected;

	case_detected = TRUE;
	if (td->single_quoting && rc->cmd[rc->i] != '\'')
		lstadd_back_char(&td->characters, rc->cmd[rc->i]);
	else if (td->single_quoting && rc->cmd[rc->i] == '\'')
		td->single_quoting = FALSE;
	else if (!td->double_quoting && rc->cmd[rc->i] == '\'')
		td->single_quoting = TRUE;
	else if (td->double_quoting && rc->cmd[rc->i] == '"')
		td->double_quoting = FALSE;
	else if (!td->double_quoting && rc->cmd[rc->i] == '"')
		td->double_quoting = TRUE;
	else
		case_detected = FALSE;
	if (case_detected)
		rc->i++;
	return (case_detected);
}

/*
** As you guessed, this is the continuation of the characters_check_1 if-else
** chain. In this one the presence of redirections, pipes and semicolons is
** done. It also will check for environment variables, $?, escaped characters
** and normal characters that didn't fit in any previous test. It returns TRUE
** if the token should NOT accept more characters.
*/

t_bool		characters_check_2(struct s_raw_command *rc,
		struct s_process_token_data *td)
{
	if (!td->double_quoting && ft_strchr("<>|;", rc->cmd[rc->i]))
	{
		if (td->first_character)
		{
			lstadd_back_char(&td->characters, '\0');
			lstadd_back_char(&td->characters, rc->cmd[rc->i]);
			if (rc->cmd[rc->i++] == '>' && rc->cmd[rc->i] == '>')
				lstadd_back_char(&td->characters, rc->cmd[rc->i++]);
		}
		return (TRUE);
	}
	else if (rc->cmd[rc->i] == '$' && (ft_isalpha(rc->cmd[rc->i + 1]) ||
				rc->cmd[rc->i + 1] == '_'))
		process_env_var(rc, &td->characters, td->env_vars);
	else if (rc->cmd[rc->i] == '$' && rc->cmd[rc->i + 1] == '?')
		process_last_exit_status(rc, &td->characters, td->last_exit_status);
	else if (rc->cmd[rc->i] == '\\' && !rc->cmd[rc->i + 1])
		set_backslash_ending(rc, td);
	else if ((!td->double_quoting || ft_strchr("$\"\\", rc->cmd[rc->i + 1])) &&
			rc->cmd[rc->i] == '\\' && rc->cmd[rc->i + 1])
		lstadd_back_char(&td->characters, rc->cmd[(rc->i += 2) - 2 + 1]);
	else
		lstadd_back_char(&td->characters, rc->cmd[rc->i++]);
	return (FALSE);
}
