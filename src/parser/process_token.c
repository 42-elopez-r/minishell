/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_token.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/28 18:09:34 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/11 17:54:14 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <common.h>
#include <get_next_line.h>
#include <parser.h>
#include <stdlib.h>

extern t_bool	g_sigint_received;

/*
** This functions reads characters from the raw command (advancing the index)
** corresponding to one token and stores them along with extra data (see
** the struct fields) in td.
*/

static void		get_token_characters(struct s_raw_command *rc,
		struct s_process_token_data *td)
{
	while (rc->cmd[rc->i] && (rc->cmd[rc->i] != ' ' ||
				td->single_quoting || td->double_quoting))
	{
		if (!characters_check_1(rc, td))
		{
			if (characters_check_2(rc, td))
			{
				td->first_character = FALSE;
				break ;
			}
		}
		td->first_character = FALSE;
	}
}

/*
** This function prompts the user for another line and keeps parsing the
** current token from it.
** If it was called because an unfinished quote and it reads an EOF, it frees
** td->characters and sets td->eof displaying an error message.
*/

static void		get_extra_line(struct s_raw_command *rc,
		struct s_process_token_data *td)
{
	free(rc->cmd);
	rc->i = 0;
	ft_putstr_fd("> ", 1);
	if (get_next_line_ctrl_d(get_stdin()->fd, &rc->cmd) &&
			(td->single_quoting || td->double_quoting))
	{
		td->eof = TRUE;
		ft_lstclear(&td->characters, free);
		ft_putstr_fd("\nminishell: unexpected EOF while looking"
				" for matching `", 1);
		ft_putstr_fd(td->single_quoting ? "'" : "\"", 1);
		ft_putendl_fd("'", 1);
		ft_putendl_fd("minishell: syntax error: unexpected end of file", 1);
		return ;
	}
	if (!td->backslash_ending)
		lstadd_back_char(&td->characters, '\n');
	else if (rc->cmd && !rc->cmd[0])
	{
		free(rc->cmd);
		rc->cmd = NULL;
	}
	if (rc->cmd)
		get_token_characters(rc, td);
}

/*
** This function initializes the fields of a struct s_process_token_data to
** its default values.
*/

static void		init_token_data(struct s_process_token_data *td,
		t_list *env_vars, int last_exit_status)
{
	td->env_vars = env_vars;
	td->last_exit_status = last_exit_status;
	td->single_quoting = FALSE;
	td->double_quoting = FALSE;
	td->first_character = TRUE;
	td->backslash_ending = FALSE;
	td->eof = FALSE;
	td->characters = NULL;
}

/*
** This function receives the raw command string and returns its first token,
** incrementing the index of the command string and processing the quotes,
** environment variables and escaping characters it finds.
** If it reads an EOF, it will set the eof parameter and return NULL.
*/

char			*process_token(struct s_raw_command *rc, t_list *env_vars,
		int last_exit_status, t_bool *eof)
{
	struct s_process_token_data td;
	char						*token;

	*eof = FALSE;
	init_token_data(&td, env_vars, last_exit_status);
	while ((td.first_character || td.single_quoting || td.double_quoting) &&
			!td.eof && !g_sigint_received)
	{
		get_token_characters(rc, &td);
		if (td.backslash_ending || td.single_quoting || td.double_quoting)
			get_extra_line(rc, &td);
	}
	if (td.eof)
	{
		*eof = TRUE;
		return (NULL);
	}
	else if (!rc->cmd)
		return (NULL);
	token = token_list_to_string(td.characters);
	if (td.characters)
		ft_lstclear(&td.characters, free);
	return (token);
}
