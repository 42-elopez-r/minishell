/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_command_table.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 00:02:33 by elopez-r          #+#    #+#             */
/*   Updated: 2021/02/04 22:59:57 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <command_table.h>
#include <stdlib.h>

/*
** This function frees the memory allocated in a s_redirection.
*/

static void	delete_redirections(void *redirection)
{
	struct s_redirection *redir;

	redir = redirection;
	if (redir)
	{
		free(redir->filename);
		free(redir);
	}
}

/*
** This function frees the memory allocated in a s_simple_command.
*/

static void	delete_simple_command(void *simple_command)
{
	struct s_simple_command	*simple_cmd;
	size_t					i;

	simple_cmd = simple_command;
	if (simple_cmd)
	{
		i = 0;
		while (simple_cmd->arguments[i])
			free(simple_cmd->arguments[i++]);
		free(simple_cmd->arguments);
		if (simple_cmd->redirections)
			ft_lstclear(&simple_cmd->redirections, delete_redirections);
		free(simple_cmd);
	}
}

/*
** This function frees the memory allocated in a pipeline.
*/

void		delete_pipeline(void *pipeline)
{
	t_list *pipel;

	pipel = pipeline;
	if (pipel)
		ft_lstclear(&pipel, delete_simple_command);
}

/*
** This function frees the memory allocated in a command table.
*/

void		delete_command_table(t_list *command_table)
{
	if (command_table)
		ft_lstclear(&command_table, delete_pipeline);
}
