/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   surround_var_value_quotes.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 21:37:12 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/20 10:51:01 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <environment_variables.h>

/*
** This function receives an environment variable and if it has a value
** (name=value) then it returns a copy of it with the value surrounded by
** quotes (name="value"). If it doesn't have value, it just returns a copy.
** Returns NULL on error.
*/

char	*surround_var_value_quotes(const char *var)
{
	char	*surrounded;
	size_t	i;

	if (!ft_strchr(var, '='))
		return (ft_strdup(var));
	if (!(surrounded = ft_calloc(sizeof(char), ft_strlen(var) + 3)))
	{
		ft_perror("Can't allocate environment variable copy");
		return (NULL);
	}
	i = 0;
	while (var[i] != '=')
	{
		surrounded[i] = var[i];
		i++;
	}
	surrounded[i] = var[i];
	surrounded[++i] = '"';
	while (var[i])
	{
		surrounded[i + 1] = var[i];
		i++;
	}
	surrounded[i + 1] = '"';
	return (surrounded);
}
