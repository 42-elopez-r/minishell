/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filter_empty_vars.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 20:41:55 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/19 20:51:20 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <environment_variables.h>

/*
** Removes all the empty variables.
*/

void	filter_empty_vars(t_list **env_vars)
{
	t_list	*iter;
	char	*name_copy;

	iter = *env_vars;
	while (iter)
	{
		if (!ft_strchr(iter->content, '='))
		{
			name_copy = ft_strdup(iter->content);
			iter = iter->next;
			delete_variable(name_copy, env_vars);
			free(name_copy);
		}
		else
			iter = iter->next;
	}
}
