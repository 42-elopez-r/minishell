/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environment_variables.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 22:41:29 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/20 11:28:41 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <environment_variables.h>
#include <common.h>
#include <stdlib.h>

/*
** Given an environment variable in name=value format, returns TRUE if the
** specified name matches with the one of the environment variable.
*/

t_bool			is_var_name(const char *env_var, const char *name)
{
	t_bool matches;
	size_t i;

	matches = TRUE;
	i = 0;
	while (matches && env_var[i] && env_var[i] != '=' && name[i])
	{
		if (env_var[i] != name[i])
			matches = FALSE;
		i++;
	}
	return (matches);
}

/*
** This function returns the value of an environment variable given its name
** and the list of environment variables. It will return a heap allocated
** copy of the value or NULL in case of error or if the variable doesn't
** exists.
*/

char			*env_variable_get_value(t_list *env_vars, const char *name)
{
	char *env_var;

	if (!env_vars)
		return (NULL);
	env_var = (char*)env_vars->content;
	if (is_var_name(env_var, name) && ft_strchr(env_var, '='))
		return (ft_strdup(ft_strchr(env_var, '=') + 1));
	else if (is_var_name(env_var, name))
		return (NULL);
	else
		return (env_variable_get_value(env_vars->next, name));
}

/*
** This function adds (or updates if it already exists) an environment variable
** given with the format name=value.
*/

void			update_var(t_list **env_vars, char *var)
{
	char	*var_name;
	size_t	var_name_size;
	t_list	*env_vars_copy;

	var_name_size = ft_strchr(var, '=') - var;
	if (!(var_name = ft_calloc(sizeof(char), var_name_size + 1)))
	{
		ft_perror("Can't allocate new environment variable name");
		return ;
	}
	ft_memmove(var_name, var, var_name_size);
	env_vars_copy = *env_vars;
	while (env_vars_copy)
	{
		if (is_var_name(env_vars_copy->content, var_name))
		{
			free(env_vars_copy->content);
			env_vars_copy->content = ft_strdup(var);
			break ;
		}
		env_vars_copy = env_vars_copy->next;
	}
	free(var_name);
	if (!env_vars_copy)
		ft_lstadd_back(env_vars, ft_lstnew(ft_strdup(var)));
}

/*
** This function increments the value of $SHLVL or initializes it to 1 if
** itsn't declared already.
*/

static void		increment_shlvl(t_list **env_vars)
{
	char *cur_shlvl;
	char *new_shlvl;
	char *new_shlvl_var;

	if ((cur_shlvl = env_variable_get_value(*env_vars, "SHLVL")))
	{
		if (!is_natural_number(cur_shlvl))
		{
			free(cur_shlvl);
			update_var(env_vars, "SHLVL=1");
			return ;
		}
		new_shlvl = ft_itoa(ft_atoi(cur_shlvl) + 1);
		free(cur_shlvl);
		new_shlvl_var = ft_strjoin("SHLVL=", new_shlvl);
		free(new_shlvl);
		update_var(env_vars, new_shlvl_var);
		free(new_shlvl_var);
	}
	else
		update_var(env_vars, "SHLVL=1");
}

/*
** Returns a list with the default environment variables.
*/

t_list			*get_env_vars(char **envp)
{
	t_list *env_vars;

	env_vars = NULL;
	if (!envp || !*envp)
	{
		env_vars = ft_lstnew(ft_strdup("PATH=/usr/local/sbin:/usr/local/bin:"
					"/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin"));
	}
	else
		env_vars = string_array_to_list(envp);
	filter_empty_vars(&env_vars);
	update_var(&env_vars, "PS1=my little shell $ ");
	reset_empty_var("OLDPWD", &env_vars);
	increment_shlvl(&env_vars);
	return (env_vars);
}
