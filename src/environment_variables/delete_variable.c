/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_variable.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 19:38:01 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/19 19:42:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <environment_variables.h>

/*
** This function removes an environment variable given its name.
*/

void	delete_variable(const char *name, t_list **env_vars)
{
	t_list	*iter;
	t_list	*prev;

	iter = *env_vars;
	prev = NULL;
	while (iter)
	{
		if (is_var_name(iter->content, name))
		{
			if (prev)
				prev->next = iter->next;
			else
				*env_vars = (*env_vars)->next;
			free(iter->content);
			free(iter);
			break ;
		}
		prev = iter;
		iter = iter->next;
	}
}
