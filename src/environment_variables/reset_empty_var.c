/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reset_empty_var.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 19:56:02 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/19 20:07:13 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <environment_variables.h>

/*
** This function deletes the given variable and then it adds it to the
** environment. You save two lines yay!
*/

void	reset_empty_var(const char *name, t_list **env_vars)
{
	delete_variable(name, env_vars);
	ft_lstadd_back(env_vars, ft_lstnew(ft_strdup(name)));
}
