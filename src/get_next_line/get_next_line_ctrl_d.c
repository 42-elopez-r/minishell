/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_ctrl_d.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/09 17:24:36 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/15 20:57:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <get_next_line.h>
#include <common.h>

/*
** The name of the function says it. Yes, this is a thing because the norm.
*/

static t_bool	free_line_and_return_false(char **line)
{
	free(*line);
	*line = NULL;
	return (FALSE);
}

/*
** This function is a wrapper over get_next_line() that won't return when
** it finds an EOF (will read again for more input) unless the EOF is the
** first thing that appears when reading the fd (in this case it would return
** TRUE and store NULL in *line, to differentiate from a "\n".
** Otherwise returns FALSE).
*/

t_bool			get_next_line_ctrl_d(int fd, char **line)
{
	int		ret;
	char	*str;
	char	*joined;

	*line = ft_strdup("");
	ret = 0;
	while (ret == 0)
	{
		ret = get_next_line(fd, &str);
		if (!str)
			return (free_line_and_return_false(line));
		joined = ft_strjoin(*line, str);
		free(str);
		free(*line);
		*line = joined;
		if (ret == 0 && ft_strlen(*line) == 0)
		{
			free(*line);
			*line = NULL;
			return (TRUE);
		}
	}
	return (FALSE);
}
