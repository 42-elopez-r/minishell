/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/04 17:18:15 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/17 19:03:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <command_table.h>
#include <environment_variables.h>
#include <parser.h>
#include <executor.h>
#include <common.h>
#include <signal.h>
#include <unistd.h>

t_list		*g_running_pids;
t_bool		g_sigint_received;
int			g_shell_return_code;

/*
** This is the signal handler. If the signal is a SIGINT (ctrl-c) and there
** aren't any childs, it will close the current file descriptor used as stdin
** so the running read() terminates.
*/

static void	signal_handler(int sig)
{
	struct s_fd	*cur_stdin;

	if (sig == SIGINT && !g_running_pids)
	{
		write(1, "\n", 1);
		cur_stdin = get_stdin();
		close(cur_stdin->fd);
		cur_stdin->is_open = FALSE;
		g_sigint_received = TRUE;
	}
}

/*
** What the function name says.
*/

static void	delete_env_vars_and_stdin(t_list **env_vars)
{
	ft_lstclear(env_vars, free);
	close(get_stdin()->fd);
	free(get_stdin());
}

int			main(int argc, char *argv[], char **environ)
{
	int		last_exit_status;
	t_list	*env_vars;
	t_list	*command_table;

	(void)argc;
	(void)argv;
	g_running_pids = NULL;
	g_shell_return_code = -1;
	signal(SIGINT, signal_handler);
	signal(SIGQUIT, signal_handler);
	last_exit_status = 0;
	env_vars = get_env_vars(environ);
	while (g_shell_return_code == -1)
	{
		g_sigint_received = FALSE;
		command_table = get_command_table(env_vars, last_exit_status);
		if (g_sigint_received)
			last_exit_status = 130;
		else
			last_exit_status = executor(command_table, &env_vars);
		delete_command_table(command_table);
	}
	delete_env_vars_and_stdin(&env_vars);
	return (g_shell_return_code);
}
