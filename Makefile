# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/03/04 17:41:59 by mzomeno-          #+#    #+#              #
#    Updated: 2021/03/15 19:40:14 by mzomeno-         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell

CC = gcc
SHELL = sh
CFLAGS += -Wall -Wextra -Werror -D $(shell uname)

INCLUDE_DIR = include/
HEADERS = $(shell find $(INCLUDE_DIR) -name "*.h")

LIBFT = libft.a
LIBFT_DIR = libft/

SRC_DIR = src/
SRC = $(shell find src -name "*.c" | xargs -I % $(SHELL) -c 'echo % | cut -c 5-')

OBJ_DIR = obj/
OBJS = $(addprefix $(OBJ_DIR), $(SRC:.c=.o))

all: $(NAME)

$(OBJ_DIR)%.o:	$(SRC_DIR)%.c
		$(CC) $(CFLAGS) -I $(INCLUDE_DIR) -c $< -o $@

$(NAME): $(OBJ_DIR) $(OBJS) $(LIBFT)
	$(CC) $(OBJS) $(LIBFT_DIR)$(LIBFT) $(CFLAGS) -o $(NAME)

$(OBJ_DIR):
		mkdir $(OBJ_DIR)
		mkdir $(OBJ_DIR)command_table
		mkdir $(OBJ_DIR)common
		mkdir $(OBJ_DIR)environment_variables
		mkdir $(OBJ_DIR)executor
		mkdir $(OBJ_DIR)get_next_line
		mkdir $(OBJ_DIR)parser
		mkdir $(OBJ_DIR)builtins
		echo object directories have been created!

$(LIBFT): $(LIBFT_DIR)
	$(MAKE) -C $(LIBFT_DIR) bonus


clean:
		@rm -rf $(OBJ_DIR)
		@$(MAKE) -C $(LIBFT_DIR) fclean

fclean:		clean
		@rm -f $(NAME)
		@echo Objects and executable file erased, bye!

re: fclean all

debug: CFLAGS += -g
debug: fclean $(NAME)
