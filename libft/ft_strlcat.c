/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 16:17:04 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/03 17:36:09 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** To anyone trying to grasp what this function does: I have no idea.
** I made the basic functioning work without much trouble, but all the
** little special cases this function must handle is a nonsense I made work
** by try and error. This also serves as a TODO for my future self to maybe
** convert this pile of crap into something well coded. Good luck.
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t i_s;
	size_t i_d;
	size_t dst_orig_len;

	dst_orig_len = ft_strlen(dst);
	if (size <= dst_orig_len)
		return (size + ft_strlen(src));
	i_d = 0;
	while (dst[i_d])
		i_d++;
	i_s = 0;
	while (src[i_s] && size > 0 && i_d < size - 1)
		dst[i_d++] = src[i_s++];
	if (size > 0)
		dst[i_d] = '\0';
	if (size > dst_orig_len)
		size = dst_orig_len;
	return (size + ft_strlen(src));
}
