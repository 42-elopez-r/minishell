/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <mzomeno-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 22:35:55 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/09 17:38:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdlib.h>
# include <libft.h>
# include <common.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 32
# endif

int			chr_pos(char *s, char c);
char		*copy_until_chr(char *s, char c);
void		trim_until_chr(char *s, char c);
int			get_next_line(int fd, char **line);
t_bool		get_next_line_ctrl_d(int fd, char **line);

#endif
