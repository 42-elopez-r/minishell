/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <mzomeno-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 18:28:43 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/20 00:34:42 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

# include <libft.h>

typedef enum	e_bool
{
	FALSE = 0,
	TRUE = 1
}				t_bool;

struct			s_fd
{
	int			fd;
	t_bool		is_open;
};

void			ft_perror(char *str);
char			*list_to_string(t_list *list);
char			**list_to_string_array(t_list *list);
t_list			*string_array_to_list(char **array);
void			lstadd_back_char(t_list **list, char c);
void			*lstpop(t_list **lst);
void			*lstindex(t_list *lst, size_t index);
void			lst_alpha_order(t_list **list);
t_list			*lst_cpy(t_list *list);
int				ft_strcmp(const char *s1, const char *s2);
struct s_fd		*get_stdin(void);
pid_t			*piddup(pid_t pid);
t_bool			is_natural_number(const char *str);

#endif
