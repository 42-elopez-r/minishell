/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 19:10:49 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/10 14:01:42 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include <command_table.h>

t_list					*get_command_table(t_list *env_vars,
		int last_exit_status);

/*
** Private structs and functions for internal parser use
*/

struct					s_raw_command
{
	char				*cmd;
	size_t				i;
};

struct					s_process_token_data
{
	t_list				*env_vars;
	int					last_exit_status;
	t_list				*characters;
	t_bool				single_quoting;
	t_bool				double_quoting;
	t_bool				first_character;
	t_bool				backslash_ending;
	t_bool				eof;
};

struct					s_tokens
{
	t_list				*toks;
	size_t				i;
};

void					process_env_var(struct s_raw_command *rc,
		t_list **characters, t_list *env_vars);
void					process_last_exit_status(struct s_raw_command *rc,
		t_list **characters, int last_exit_status);
char					*token_list_to_string(t_list *characters);
t_bool					characters_check_1(struct s_raw_command *rc,
		struct s_process_token_data *td);
t_bool					characters_check_2(struct s_raw_command *rc,
		struct s_process_token_data *td);
char					*process_token(struct s_raw_command *rc,
		t_list *env_vars, int last_exit_status, t_bool *eof);
t_list					*parse_tokens(struct s_raw_command *rc,
		t_list *env_vars, int last_exit_status);
char					*cur_token(struct s_tokens *tokens);
char					*cur_token_off(struct s_tokens *tokens, size_t offset);
struct s_simple_command	*process_simple_command(struct s_tokens *tokens);
t_list					*tokens_to_command_table(struct s_tokens *tokens);

#endif
