/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_table.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 18:09:48 by elopez-r          #+#    #+#             */
/*   Updated: 2021/02/04 22:54:39 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMAND_TABLE_H
# define COMMAND_TABLE_H

# include <common.h>
# include <libft.h>

struct		s_redirection
{
	char	*filename;
	t_bool	is_input;
	t_bool	overwrite;
};

struct		s_simple_command
{
	char	**arguments;
	t_list	*redirections;
};

void		delete_pipeline(void *pipeline);
void		delete_command_table(t_list *command_table);

#endif
