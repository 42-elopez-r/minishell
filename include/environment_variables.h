/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environment_variables.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 22:38:59 by elopez-r          #+#    #+#             */
/*   Updated: 2021/03/19 23:48:07 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENVIRONMENT_VARIABLES_H
# define ENVIRONMENT_VARIABLES_H

# include <common.h>

t_bool	is_var_name(const char *env_var, const char *name);
char	*env_variable_get_value(t_list *env_vars, const char *name);
t_list	*get_env_vars(char **envp);
void	update_var(t_list **env_vars, char *var);
void	delete_variable(const char *name, t_list **env_vars);
void	reset_empty_var(const char *name, t_list **env_vars);
void	filter_empty_vars(t_list **env_vars);
char	*surround_var_value_quotes(const char *var);

#endif
