/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   executor.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <mzomeno-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/29 10:40:50 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 00:38:07 by mzomeno-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EXECUTOR_H
# define EXECUTOR_H

# include <sys/types.h>
# include <common.h>
# include <command_table.h>
# include <builtins.h>

# define EMPTY_STDIN -16

struct		s_pipes
{
	int		odd_pipe[2];
	int		even_pipe[2];
};

struct		s_loop_control
{
	t_bool	first;
	t_bool	last;
};

t_bool		find_command(char *path, char *command);
t_bool		search_paths(struct s_simple_command *simple_cmd,
			char **cmd_path, char **cmd, t_list *env_vars);
char		*validate_command(struct s_simple_command *simple_cmd,
			t_list **env_vars, t_builtin *builtin);
int			get_infd(t_list *redir_list, struct s_loop_control *positions,
			int last_pipe);
int			get_outfd(t_list *redir_list, struct s_loop_control *positions,
			int pipe_out[2]);
pid_t		run_child_process(int pipe_out[2], int fds_in_out[2],
								struct s_simple_command *simple_cmd,
								t_list **env_vars);
void		execute_command(struct s_simple_command *simple_cmd,
			t_list **env_vars);
void		redirect_io(int fdin, int fdout, int read_end);
int			redirect_pipes(struct s_simple_command *simple_cmd,
			struct s_loop_control *positions, struct s_pipes *pipes,
			int *stdfd);
char		**get_paths(t_list *env_vars);
void		delete_paths(char **paths);
pid_t		process_pipeline(t_list *pipeline, struct s_loop_control *positions,
			int *pipe_in, t_list **env_vars);
int			executor(t_list *command_table, t_list **env_vars);

#endif
