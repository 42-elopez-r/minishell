/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mzomeno- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/12 11:56:36 by mzomeno-          #+#    #+#             */
/*   Updated: 2021/03/20 11:01:51 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTINS_H
# define BUILTINS_H

# include <common.h>

typedef int		(*t_builtin)(char**, t_list**);

t_builtin		search_builtin(char *cmd);

int				exec_echo(char **args, t_list **env_vars);
int				exec_cd(char **args, t_list **env_vars);
int				exec_pwd(char **args, t_list **env_vars);
int				exec_export(char **args, t_list **env_vars);
int				exec_unset(char **args, t_list **env_vars);
int				exec_env(char **args, t_list **env_vars);
int				exec_exit(char **args, t_list **env_vars);
int				export_nothing(t_list **env_vars);
int				export_something(char **args, t_list **env_vars);

#endif
